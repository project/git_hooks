A collection of useful git hooks.

pre-commit hook - works on local repository.

pre-commit: A wrapper hook that lives in and looks in the
.git/hooks/pre-commit_* dirs for other scripts and runs those hooks.
pre-commit_coding_standards/pre-commit_phpcs: verify the commited code against
drupal coding standards


post-receive hook - works on remote repository.

post-receive: A wrapper hook that lives in and looks in the
.git/hooks/post-receive_* dirs for other scripts and runs those hooks.
post-receive_coding_standards/post-receive_phpcs: verify the commited code
against drupal coding standards


Installation and Usage

Requires phpcs and drupalcodesniffer. for installation of those see:
http://drupal.org/project/drupalcs

Use git to clone this repository and copy the pre-commit_* files and directories
into your projects .git/hooks/ directory.
Now when you run git commit the pre-commit script will be executed, it will find
the pre-commit_phpcs script and execute that.
The pre-commit_phpcs script will analyse relevant committed files against the
Drupal coding standard and feedback errors and warnings.
In the event that you wish to commit your files regardless of the standards
errors do git commit --no-verify and git will not execute the pre-commit hooks

Copy the post-receive_* files and directories into your remote repository
.git/hooks/ directory.
post-receive hook will run every time you commit to remote repository.
He does not interrupt the process of adding files. It only checks them if it
finds errors, it sends an error report by e-mail specified in the settings of
the script. This is useful in cases where a senior developer controls the
quality of the code other developers on the project.

Script post-receive_phpcs used on the remote repository, requires a mandatory
settings.

To the git could run a scripts and hooks, you should set permissions for files
"Allow run file as program"
